from unittest import TestCase, main
from hypothesis import given, note
from hypothesis.strategies import lists, sampled_from

from gameOfLife.cell import Cell


class TestEncoding(TestCase):

    @staticmethod
    def build_data():
        neighbours = [Cell([]) for x in range(8)]
        cell = Cell(neighbours)
        return cell, neighbours

    @given(lists(elements=sampled_from(range(8)), min_size=0, max_size=1))
    def test_starves_with_less_than_two_live_neighbours(self, values):
        (cell, neighbours) = self.build_data()

        cell.make_alive()
        for x in values:
            neighbours[x].make_alive()
        note(f"Failing values: {values}")

        cell.change_state()
        self.assertEqual(Cell.DEAD, cell.state())

    @given(lists(elements=sampled_from(range(8)), min_size=3, max_size=3, unique=True))
    def test_becomes_alive_with_three_live_neighbours(self, values):
        (cell, neighbours) = self.build_data()

        for x in values:
            neighbours[x].make_alive()
        note(f"Failing values: {values}")

        cell.change_state()
        self.assertEqual(Cell.ALIVE, cell.state())

    @given(lists(elements=sampled_from(range(8)), min_size=4, max_size=8, unique=True))
    def test_overcrowded_with_more_than_three_live_neighbours(self, values):
        (cell, neighbours) = self.build_data()

        for x in values:
            neighbours[x].make_alive()
        note(f"Failing values: {values}")

        cell.make_alive()
        cell.change_state()
        self.assertEqual(Cell.DEAD, cell.state())


if __name__ == '__main__':
    main()
